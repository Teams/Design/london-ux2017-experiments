# License

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

When attributing this work, using "GNOME Project" is enough. 

The following files are not covered by this license:

* first-run/EOS_Walkthrough_06_abridged.png
* lock-login/experiments/GNOME_hackfest_0005_lock_exp_01.png
* lock-login/experiments/GNOME_hackfest_0005_lock_exp_02.png
* lock-login/experiments/GNOME_hackfest_0005_lock_exp_03.png
* lock-login/experiments/GNOME_hackfest_0005_lock_exp_04.png
* lock-login/experiments/GNOME_hackfest_0005_lock_exp_05.png