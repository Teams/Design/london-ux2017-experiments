var agata, animSettings, avatar, back, bg, bottombuttons, h, loginicons, notifications, password, pavel, tileAnimOptions, tileHidden, tileShadow, topbar, username, w, wallpaper;

w = 1366;

h = 768;

tileShadow = {
  y: 2,
  blur: 3,
  color: "rgba(0,0,0,0.2)"
};

tileAnimOptions = {
  time: 0.3,
  curve: Spring({
    damping: 0.75
  })
};

tileHidden = {
  scale: 0.1,
  opacity: 0.2
};

bg = new Layer({
  width: w,
  height: h,
  image: "images/bg.png"
});

bg.center();

bottombuttons = new Layer({
  parent: bg,
  width: w,
  height: h,
  image: "images/bottombuttons.png"
});

pavel = new Layer({
  parent: bg,
  width: 220,
  height: 220,
  x: Align.center,
  y: 274,
  borderRadius: 9,
  image: "images/pavel.png",
  shadow1: tileShadow,
  animationOptions: tileAnimOptions
});

agata = new Layer({
  parent: bg,
  width: 220,
  height: 220,
  x: Align.center(250),
  y: 274,
  borderRadius: 9,
  image: "images/agata.png",
  shadow1: tileShadow,
  animationOptions: tileAnimOptions
});

wallpaper = new Layer({
  parent: bg,
  width: w,
  height: h,
  image: "images/wallpaper.png",
  shadow1: {
    y: 2,
    blur: 3,
    color: "rgba(0,0,0,0)"
  }
});

wallpaper.states.grid = {
  parent: bg,
  width: 220,
  height: 220,
  x: 327,
  y: 274,
  borderRadius: 9,
  shadow1: {
    color: "rgba(0,0,0,0.2)"
  }
};

avatar = new Layer({
  parent: wallpaper,
  width: 160,
  height: 160,
  x: Align.center,
  y: Align.center(-87),
  borderRadius: 300,
  image: "images/avatar.png",
  shadow1: {
    y: 2,
    blur: 4,
    color: "rgba(0,0,0,0.5)"
  }
});

avatar.states.grid = {
  width: 101,
  height: 101,
  x: 60,
  y: 40,
  shadow1: {
    color: "rgba(0,0,0,0)"
  }
};

username = new Layer({
  parent: bg,
  width: 200,
  height: 30,
  x: Align.center,
  y: Align.center(25),
  image: "images/username.svg"
});

username.states.grid = {
  width: 142.5,
  height: 38,
  x: 362,
  y: Align.center(65.5),
  style: {
    "font-size": "16px"
  }
};

back = new Layer({
  parent: bg,
  width: 32,
  height: 32,
  x: Align.center(-115),
  y: Align.center(23),
  image: "images/back.svg",
  opacity: 1
});

back.states.grid = {
  width: 10,
  height: 10,
  x: 350,
  y: Align.center(100),
  opacity: 0
};

password = new Layer({
  parent: bg,
  width: 260,
  height: 34,
  x: Align.center,
  y: Align.center(75),
  image: "images/pw.png",
  opacity: 1
});

password.states.grid = {
  width: 75.2,
  height: 10.5,
  x: 400,
  y: 449,
  opacity: 0
};

loginicons = new Layer({
  parent: bg,
  width: 55,
  height: 24,
  x: Align.center,
  y: Align.center(135),
  image: "images/loginicons.svg",
  opacity: 1
});

loginicons.states.grid = {
  width: 18.3,
  height: 8,
  x: 428,
  y: Align.center(115),
  opacity: 0
};

notifications = new Layer({
  parent: bg,
  width: w,
  height: h,
  image: "images/notifications.png",
  opacity: 1
});

notifications.states.grid = {
  width: 391,
  height: 220,
  x: 241,
  y: 274,
  opacity: 0
};

topbar = new Layer({
  parent: bg,
  width: w,
  height: h,
  image: "images/top-bar.png"
});

wallpaper.onTap(function() {
  wallpaper.animate("default");
  avatar.animate("default");
  username.animate("default");
  back.animate("default");
  password.animate("default");
  loginicons.animate("default");
  return notifications.animate("default");
});

back.onTap(function() {
  wallpaper.animate("grid");
  avatar.animate("grid");
  username.animate("grid");
  back.animate("grid");
  password.animate("grid");
  loginicons.animate("grid");
  return notifications.animate("grid");
});

animSettings = {
  time: 0.4,
  curve: Spring({
    damping: 0.75
  })
};

wallpaper.animationOptions = animSettings;

avatar.animationOptions = animSettings;

username.animationOptions = animSettings;

back.animationOptions = animSettings;

password.animationOptions = animSettings;

loginicons.animationOptions = animSettings;

notifications.animationOptions = animSettings;
